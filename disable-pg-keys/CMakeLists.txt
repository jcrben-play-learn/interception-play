cmake_minimum_required(VERSION 3.0)

project(x2y)

add_executable(x2y x2y.c)
target_compile_options(x2y PRIVATE -Wall -Wextra)

install(TARGETS x2y RUNTIME DESTINATION bin)
