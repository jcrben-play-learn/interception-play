
#include <stdio.h>
#include <stdlib.h>
#include <linux/input.h>

int main(void) {
    struct input_event event;

    setbuf(stdin, NULL), setbuf(stdout, NULL);

    fprintf( stderr, "that didn't go well\n" );

    FILE *f = fopen("/home/ben/file.txt", "w");
    if (f == NULL)
    {
        printf("Error opening file!\n");
        exit(1);
    }

    /* print some text */
    const char *text = "Write this to the file";
    fprintf(f, "Some text: %s\n", text);

    /* print integers and floats */
    int i = 1;
    float py = 3.1415927;
    fprintf(f, "Integer: %d, float: %f\n", i, py);

    /* printing single chatacters */
    char c = 'A';
    fprintf(f, "A character: %c\n", c);

    fclose(f);

    printf( "hello world\n" );

    while (fread(&event, sizeof(event), 1, stdin) == 1) {
        // if (event.type == EV_KEY && event.code == KEY_X)
        //     event.code = KEY_Y;

        fprintf( stderr, "that didn't go well\n" );

        FILE *f = fopen("/home/ben/file.txt", "w");
        if (f == NULL)
        {
            printf("Error opening file!\n");
            exit(1);
        }

        /* print some text */
        const char *text = "Write this to the file";
        fprintf(f, "Some text: %s\n", text);

        /* print integers and floats */
        int i = 1;
        float py = 3.1415927;
        fprintf(f, "Integer: %d, float: %f\n", i, py);

        /* printing single chatacters */
        char c = 'A';
        fprintf(f, "A character: %c\n", c);

        fclose(f);

        printf( "hello world\n" );

        fwrite(&event, sizeof(event), 1, stdout);
    }
}